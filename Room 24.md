# Lost Librarians

These eight librarians were storing their copies of books in the records cupboard (room 27), and did not notice the rooms were defrosting until it was too late.

They have not dressed appropriately, so they are all very cold.
They each have 10 of their favourite books in hand, and will be hard-pressed to leave without guaranteeing the safety of those books.
