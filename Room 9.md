# The Office

This flooded room has a desk and a great many shelves detailing almost all inventory in the area, such as the estimated value of the books in the Giftshrank (room 26).

It also contains a ledger of everyone who has passed in or out.

It does not contain any information on the hidden door in room 15.
