This is a project space for an open-source dungeon, created by Discord's OSR community.

## Dungeon Overview

This area started as a simple fridge, deep underground, but as the city grew various interested parties have expanded it repeatedly.

Now, a particularly hot Summer has come and a tipping point has been reached.
The 'Ice Caves' (as the locals call them) are melting.

The PCs may want to rescue people, or treasure, or just make sure that dragon doesn't thaw out!

## Rough Plan

- [X] Agree on a dungeon idea on the [issues board](https://gitlab.com/bindrpg/gnu-dun/-/boards).
- [X] Agree on a map.
    * Rough draft [here](https://gitlab.com/bindrpg/gnu-dun/-/issues/2)
- [ ] Everyone write in markdown until we have interesting rooms.
    * [ ] Salvage CC0 art online?
- [ ] Formatting/ layout
- [ ] Polish
    * [ ] Editing
    * [ ] Playtesting!
    * [ ] Repeat

### Rooms To Do

Check out the [map](https://gitlab.com/bindrpg/gnu-dun/-/issues/2) for the key.

- [X] Room 1
- [ ] Room 2
- [ ] Room 3
- [ ] Room 4
- [ ] Room 5
- [ ] Room 6
- [X] Room 7
- [ ] Room 8
- [X] Room 9
- [ ] Room 10
- [ ] Room 11
- [X] Room 12
- [X] Room 13
- [X] Room 14
- [X] Room 15
- [X] Room 16
- [X] Room 17
- [ ] Room 18
- [ ] Room 19
- [X] Room 20
- [X] Room 21
- [ ] Room 22
- [ ] Room 23
- [X] Room 24
- [ ] Room 25
- [X] Room 26
- [ ] Room 27
- [X] Room 28

### Joining

For the timid, [email](mailto:incoming+bindrpg-gnu-dun-44312544-3v2b5r7lcdiaijiaslvavggax-issue@incoming.gitlab.com) an idea or issue.

For everyone else,

1. sign up to Gitlab,
1. [send us your username](mailto:incoming+bindrpg-gnu-dun-44312544-3v2b5r7lcdiaijiaslvavggax-issue@incoming.gitlab.com) so we can add you,
1. click here to open a [Web IDE](https://gitlab.com/-/ide/project/bindrpg/gnu-dun/edit/dev/-/) in the `dev`elopment version (or click `+` to make your own branch),
1. and start putting in those room descriptions (or anything else you want to see).

## System

It'll start without any system.
If someone wants to add a particular system, we can add it in a branch (and have multiple systems going at once).
