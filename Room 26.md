# Giftschrank

This old collection of banned (and rather dusty) books contains a wide variety of books and pamphlets.

- *Jane's Lewd Limericks, Volume IV* (3 sp)
- *On the Metaphysics of Government as a Super Organizm* (10 gp, but buyers are hard to find)
- *The Sexual Lives of Gnolls* (1 gp)
- *Areginal Thought* (5 sp)
- *Jane's Vile Verses, Volume VIII* (2 sp)
- *An Abridged History of Warfare* (1 gp)
- *Life in an Elven Stronghold* (2 sp)
- *The Daring Adventures of Dingus the Dullard* (3 cp)
