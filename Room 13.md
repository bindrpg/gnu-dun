# The Far Shore

James, the local guard, stands on the far shore, shouting for help and waving his torch so frantically that it goes out.

James wasn't meant to be here today.
He took Quentin's shift so that Quentin could attend a wedding.
Could this mean that Quentin orchestrated this disaster for his own benefit, and planned to take this time off by lying about the wedding?
No.

Once the PCs reach the shore, James is in room 20, trying to work a tinder box to relight his torch.

James is a conspiracy nut, and will stall the PCs by telling them fascinating theories.

- *We don't use half the umber hulks and other hibernating monsters down here. The people who keep the poorhouses spend the money on themselves and grind up the massive insects down here to give to the poor as gruel.*
- *Ancient magical artefacts, from before when the world was hot, had to be burried down here where it's cold, to keep them comfortable. Some of the artefacts are actually sentient! That's what a drunken alchemist told me once.*
- *The world is actually a giant ball, but you can't see how it's curved because it's so big that it looks flat!*

**If the PCs ask him where any treasures are,**
he refuses to tell them until they get him out to safety.
At this point, he tells them that there is a secret door in room 15, but only after he has explained *all* of his other conspiracies on the way up.
