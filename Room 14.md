# Funerary Vault

Two massive marble doors slide to either side to retain the cold. The dead may rest in cold solace, the natural chill of this chamber keeping them inert.
Several cold metal slabs and marble containers line the walls and sides of the chamber. 

The *dead stir* as the chamber thaws. 

Enter through the stairs up from 12 in the southwest, door to 15 in the North West goes to an overflow vault.
