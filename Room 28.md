# Frozen Blood Samples
## Overview
This room houses several samples of frozen vampire, and other monsterous blood.  
The room is perfectly square, with one side containing an entire shelf of beakers, each filled with strange liquids.
They are labeled with various creature names.
What the labels neglect to mention is that every creature who's blood is present had been vampirized before the sample was taken.

At the front of the room there is a small desk and a rotting diary.
On the other wall there is one large beaker, containing 15 gallons of a bright red liquid.

The bones of a dead scientist are on the ground, along with some broken glass (the beaker he drank from before he died)

## Trap 
The door to this room has a chemical blast trap.  Unfortunately the chemicals have long reacted, and so now, if not disarmed instead of an acid attack, it releases a 15' cone of fire  and shoot sparks in all directions - igniting the wood shelf that contains the blood samples

### Scenario A - The players Disarm the Trap
All good

### Scenario B - the woodshelf catches fire.  
Players can try in a plausible way to put out the fire.  If hey do, only one sample thaws out and attacks the players.  If the players fail the check, 4 samples thaw out and attack the players (whilst the others evaporate)

#### Combat
During the first round the vampire oozes will attack like liquidform versions of their living forms.  One will fly and swoop down to attack, one will bite at the players heels, one will pounce like a lion and one will take a humanoid shape.  

The oozes will fight seperately (using the stats for their respective animals) until one of them is slain or round 3 (whichever comes first), then they will spend their turns to join together.  If at least three of them are able to fuse they become a Vampiric Ooze

If the players find the diary and translate it, the book and determine that the large red sample, is intended for grand vampirism, but the effects currently have not been made permenant. 

A PC who drinks from the sample is seriously injured.
For the next hour the PC has a bite attack which heals them if they successfully bit someone.
The player takes double damage from light-based sources damage and is staggered if exposed to sunlight.  

## The Scientist
If any PC casts a spell to speak with dead, the scientist bones will admit that he is the author of the diary and can translate it for the them.
However he warns that his quest to "perfect the human body" brought only pain and suffering and will ask the PCs to destory his work.
If they do his soul rests in peace and the gods cast a blessing on the party as a reward.
