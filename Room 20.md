# The Guard Station

The door here is made with reinforced brass, and has a key.

Here, guards stand watch over the creatures kept hibernating, surrounded by ice.
They all dress in extremely warm clothes, and may only use alcohol-lanterns to see and keep warm.

The entrance room contains a weapons rack, with four spears, and four gladiatorial nets.
A thick curtain cuts the room in half, which keeps the heat away from the hibernation cell (room 21).

In the room's second half, a murder-hole allows anyone to see outside, although onlookers cannot see much except people torchlight.

## Heat Level 2

Once the room heats up more, the brass door's lock will expand, so nobody will be able to turn the key. It will remain unlocked (or locked, if it was locked).
