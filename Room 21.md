# The Hibernation Cell

> Some years ago, a dragon named 'Sai' attacked the city. She was 'defeated' by being placed in a magical sleep, but instead of killing her, the local lord declared she should be put 'on ice', with the other fantastic creatures he keeps below.

> Nobody knows whether or not the dragon is still alive, only that she was packed in ice.  Since then, the guards have continued piling hibernating creatures into the hole, to be taken out when the Monster Arena needs another creature to kill.

The brass-reinforced door has a complex locking mechanism consisting of three locks, and the change in temperature has malformed the delicate third lock, so it no longer functions.

Inside, the room contains various hibernating creatures, packed in ice and straw:

- 4 umber hulks
- 2 giant spiders
- 1 rust monster
- Sai the Dragon

Once the temperature rises, all of them wake up.
Sai, however, cannot move until some of the compact ice around her has melted.
