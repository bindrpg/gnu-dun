When the town became a city, the fridge became 'the Ice Caves'.  People used it as temporary storage of the dead, a place to keep illegal books, food storage for the town, and a place to stash hibernating umber hulks, so the masses can enjoy their bloodsports.

Now Summer has come, the caves are melting, time is running short, people are crying for rescue, there is only one boat, the walls are crumbling, the ceiling is falling in, and a conspiracy nut wants you to stop and listen to his theories.

