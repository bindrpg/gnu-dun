# The Near Shore

This room is covered in a forest of pale pillars which hold the ceiling up.

In the distance, a man can be heard shouting for help, from area 13 on the far shore.

### Low Tide

Entering the water will freeze anyone, giving them a -2 penalty to all physical actions until they warm up.

### High Tide

A barrel of ink breaks, making all the water black.

The dead have escaped from the mortuary (room ??), and wade through the frigid waters.

**Once the PCs enter the water,**
the dead get a *Sneak Attack*.
